//
//  HistoryCollectionViewCell.swift
//  FacebookAppDesing
//
//  Created by Jorge luis Menco Jaraba on 8/03/20.
//  Copyright © 2020 Jorge luis Menco Jaraba. All rights reserved.
//

import UIKit

class HistoryCollectionViewCell: UICollectionViewCell {

    lazy var historyPhotoView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    lazy var historyPhoto: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = ContentMode.scaleAspectFill
        imageView.layer.cornerRadius = 10
        imageView.clipsToBounds = true
        
        return imageView
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraintsHistoryCell()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        super.prepareForReuse()
    }

    private func setupConstraintsHistoryCell() {
        contentView.addSubview(historyPhotoView)
        NSLayoutConstraint.activate([
            historyPhotoView.topAnchor.constraint(equalTo: contentView.topAnchor),
            historyPhotoView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            historyPhotoView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            historyPhotoView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])
        
        historyPhotoView.addSubview(historyPhoto)
        NSLayoutConstraint.activate([
            historyPhoto.topAnchor.constraint(equalTo: historyPhotoView.topAnchor),
            historyPhoto.leadingAnchor.constraint(equalTo: historyPhotoView.leadingAnchor),
            historyPhoto.trailingAnchor.constraint(equalTo: historyPhotoView.trailingAnchor),
            historyPhoto.bottomAnchor.constraint(equalTo: historyPhotoView.bottomAnchor)
        ])
    }
    
    func setupCell(title: String) {
        historyPhoto.clipsToBounds = true
        historyPhoto.layer.cornerRadius = 10
        historyPhoto.image = UIImage(named: title)
    }
}
