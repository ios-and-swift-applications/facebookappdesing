//
//  NewsCollectionViewCell.swift
//  FacebookAppDesing
//
//  Created by Jorge luis Menco Jaraba on 8/03/20.
//  Copyright © 2020 Jorge luis Menco Jaraba. All rights reserved.
//

import UIKit

class NewsCollectionViewCell: UICollectionViewCell {
    lazy var headerContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    lazy var profilePicture: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.backgroundColor = .black
        image.clipsToBounds = true
        image.layer.cornerRadius = image.frame.size.width / 2
        return image
    }()

    lazy var userNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Jorge Luis Menco"
        return label
    }()

    lazy var timeSpam: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "8 minutos Ago"
        return label
    }()

    lazy var middleContainer: UIView = {
        let view = UIView()
        view.backgroundColor = .brown
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    lazy var footerContainer: UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    lazy var reactionsStack: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()

    lazy var reactionImage: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()

    lazy var commentsLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    lazy var actionStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()

    lazy var likeContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    lazy var commentContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    lazy var shareContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    lazy var newsLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()

    lazy var historyPhoto: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = ContentMode.scaleAspectFill
        imageView.layer.cornerRadius = 10
        imageView.clipsToBounds = true

        return imageView
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraintsNewsCell()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        super.prepareForReuse()
    }

    private func setupConstraintsNewsCell() {
        contentView.addSubview(headerContainer)
        contentView.addSubview(middleContainer)
        contentView.addSubview(footerContainer)

        NSLayoutConstraint.activate([
            headerContainer.topAnchor.constraint(equalTo: contentView.topAnchor),
            headerContainer.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            headerContainer.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            headerContainer.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.20)
            ])

        NSLayoutConstraint.activate([
            middleContainer.topAnchor.constraint(equalTo: headerContainer.bottomAnchor, constant: 5.0),
            middleContainer.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            middleContainer.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            middleContainer.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.60)
            ])
        NSLayoutConstraint.activate([
            footerContainer.topAnchor.constraint(equalTo: middleContainer.bottomAnchor, constant: 5.0),
            footerContainer.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            footerContainer.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            footerContainer.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.20)
            ])

        setupNewsHeader()
    }

    private func setupNewsHeader() {
        headerContainer.addSubview(profilePicture)
        headerContainer.addSubview(userNameLabel)
        headerContainer.addSubview(timeSpam)

        NSLayoutConstraint.activate([
            profilePicture.topAnchor.constraint(equalTo: headerContainer.topAnchor, constant: 5.0),
            profilePicture.leadingAnchor.constraint(equalTo: headerContainer.leadingAnchor, constant: 5.0),
            profilePicture.widthAnchor.constraint(equalToConstant: 40.0),
            profilePicture.heightAnchor.constraint(equalToConstant: 40.0)
            ])

        NSLayoutConstraint.activate([
            userNameLabel.topAnchor.constraint(equalTo: headerContainer.topAnchor, constant: 7.0),
            userNameLabel.leadingAnchor.constraint(equalTo: profilePicture.trailingAnchor, constant: 3.0),
            ])

        NSLayoutConstraint.activate([
            timeSpam.topAnchor.constraint(equalTo: userNameLabel.bottomAnchor, constant: 1.0),
            timeSpam.leadingAnchor.constraint(equalTo: profilePicture.trailingAnchor, constant: 3.0),
            ])
    }

    func setupCell(title: String) {
        newsLabel.text = title
    }
}
