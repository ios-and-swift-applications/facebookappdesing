//
//  LoginViewController.swift
//  FacebookAppDesing
//
//  Created by Jorge luis Menco Jaraba on 2/02/20.
//  Copyright © 2020 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation
import UIKit


class LoginViewController: UIViewController {

    lazy var mainStak: UIStackView = {
        let stack = UIStackView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .vertical
        stack.distribution = .fill
        return stack
    } ()

    lazy var headerImageContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        return view
    }()

    lazy var headerLoginImage: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.image = UIImage(named: "fcebookImage.png")
        image.contentMode = .scaleAspectFit
        return image
    }()

    //MARK: - Login elements
    lazy var mainContainer: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    lazy var loginContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    lazy var credentialsStack: UIStackView = {
        let stack = UIStackView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .vertical
        stack.distribution = .fill
        return stack
    }()

    lazy var userName: UITextField = {
        let textfield = UITextField()
        textfield.translatesAutoresizingMaskIntoConstraints = false
        textfield.placeholder = "jorge@1234.com"
        textfield.text  = "Jorge"
        return textfield
    }()

    lazy var password: UITextField = {
        let textfield = UITextField()
        textfield.translatesAutoresizingMaskIntoConstraints = false
        textfield.placeholder = "1234"
        textfield.text = "123"
        return textfield
    }()

    lazy var logInButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(login), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = UIColor(red: 22 / 255, green: 118 / 255, blue: 242 / 255, alpha: 1)
        button.titleLabel?.textColor = .white
        button.titleLabel?.font = .boldSystemFont(ofSize: 14.0)
        button.setTitle("Iniciar sesión", for: .normal)
        return button
    }()

    lazy var forgotPasswordLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor(red: 126 / 255, green: 156 / 255, blue: 185 / 255, alpha: 1)
        label.font = .boldSystemFont(ofSize: 14.0)
        label.text = "¿Olvide mi contraseña?"
        return label
    }()

//MARK: - footer elements
    lazy var footerContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        return view
    }()

    lazy var createAccountContainer: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.masksToBounds = true
        view.layer.cornerRadius = 3.0
        view.backgroundColor = UIColor(red: 233 / 255, green: 243 / 255, blue: 255 / 255, alpha: 1)
        return view
    }()

    lazy var createAccountLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Crear cuenta nueva"
        label.textColor = UIColor(red: 126 / 255, green: 156 / 255, blue: 185 / 255, alpha: 1)
        label.font = .boldSystemFont(ofSize: 14.0)
        return label
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func loadView() {
        super.loadView()
        self.setupView()
    }

    private func setupView() {
        self.addElementsToView()
        self.setupMainStackConstraints()
        self.setupHeaderContainer()
        self.setupLoginContainer()
        self.setupLoginElements()
        self.setupFooterContainer()
        self.setupFooterElements()
    }

    private func addElementsToView() {
        self.headerImageContainer.addSubview(self.headerLoginImage)
        self.mainStak.addArrangedSubview(self.headerImageContainer)
        self.mainStak.addArrangedSubview(mainContainer)
        self.mainStak.addArrangedSubview(footerContainer)

        self.view.addSubview(self.mainStak)
    }

    private func setupMainStackConstraints() {
        NSLayoutConstraint.activate([
            mainStak.leadingAnchor.constraint(equalToSystemSpacingAfter: self.view.leadingAnchor, multiplier: 0.0),
            mainStak.trailingAnchor.constraint(equalToSystemSpacingAfter: self.view.trailingAnchor, multiplier: 0.0),
            mainStak.topAnchor.constraint(equalToSystemSpacingBelow: self.view.topAnchor, multiplier: 0.0),
            mainStak.bottomAnchor.constraint(equalToSystemSpacingBelow: self.view.bottomAnchor, multiplier: 0.0)
            ])
    }

    private func setupHeaderContainer() {
        NSLayoutConstraint.activate([
            headerImageContainer.widthAnchor.constraint(equalTo: self.mainStak.widthAnchor, multiplier: 1.0),
            headerImageContainer.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.3)
            ])

        NSLayoutConstraint.activate([
            headerLoginImage.widthAnchor.constraint(equalTo: self.headerImageContainer.widthAnchor, multiplier: 1.0),
            headerLoginImage.heightAnchor.constraint(equalTo: self.headerImageContainer.heightAnchor, multiplier: 1.0)
            ])
    }

    private func setupLoginContainer() {
        NSLayoutConstraint.activate([
            mainContainer.widthAnchor.constraint(equalTo: self.mainStak.widthAnchor, multiplier: 1.0),
            mainContainer.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.5)
            ])
    }

    private func setupLoginElements() {
        self.mainContainer.addSubview(loginContainer)

        NSLayoutConstraint.activate([
            loginContainer.leadingAnchor.constraint(equalTo: self.mainContainer.leadingAnchor, constant: 24),
            loginContainer.trailingAnchor.constraint(equalTo: self.mainContainer.trailingAnchor, constant: -24),
            loginContainer.topAnchor.constraint(equalTo: self.mainContainer.topAnchor, constant: 30),
            loginContainer.heightAnchor.constraint(equalTo: self.mainContainer.heightAnchor, multiplier: 0.6)
            ])

        self.loginContainer.addSubview(credentialsStack)
        self.loginContainer.addSubview(logInButton)
        self.loginContainer.addSubview(forgotPasswordLabel)

        self.credentialsStack.addArrangedSubview(userName)
        self.credentialsStack.addArrangedSubview(password)

        setupCredentialStack()
        setupTexFields()
        setupLoginButton()
        setupForgoPasswordLabel()
    }

    private func setupCredentialStack() {
        NSLayoutConstraint.activate([
            credentialsStack.leadingAnchor.constraint(equalTo: self.loginContainer.leadingAnchor, constant: 0),
            credentialsStack.trailingAnchor.constraint(equalTo: self.loginContainer.trailingAnchor, constant: 0),
            credentialsStack.topAnchor.constraint(equalToSystemSpacingBelow: self.loginContainer.topAnchor, multiplier: 0)
            ])
    }

    private func setupTexFields() {
        NSLayoutConstraint.activate([
            userName.heightAnchor.constraint(equalToConstant: 45),
            password.heightAnchor.constraint(equalToConstant: 45)
            ])

        userName.layer.borderWidth = 0.5
        userName.layer.borderColor = UIColor.gray.cgColor
        userName.layer.masksToBounds = true
        userName.layer.cornerRadius = 2

        password.layer.borderWidth = 0.5
        password.layer.borderColor = UIColor.gray.cgColor
        password.layer.masksToBounds = true
        password.layer.cornerRadius = 2
    }

    private func setupLoginButton() {
        logInButton.layer.masksToBounds = true
        logInButton.layer.cornerRadius = 5.0
        NSLayoutConstraint.activate([
            logInButton.widthAnchor.constraint(equalTo: self.loginContainer.widthAnchor),
            logInButton.topAnchor.constraint(equalTo: credentialsStack.bottomAnchor, constant: 10),
            logInButton.heightAnchor.constraint(equalToConstant: 40),
            logInButton.centerXAnchor.constraint(equalTo: self.mainContainer.centerXAnchor)
            ])
    }

    private func setupForgoPasswordLabel() {
        NSLayoutConstraint.activate([
            forgotPasswordLabel.centerXAnchor.constraint(equalTo: self.loginContainer.centerXAnchor),
            forgotPasswordLabel.topAnchor.constraint(equalTo: logInButton.bottomAnchor, constant: 20),
            ])
    }

    private func setupFooterElements() {
        setupCreateAccount()
    }

    private func setupFooterContainer() {
        NSLayoutConstraint.activate([
            footerContainer.widthAnchor.constraint(equalTo: self.mainStak.widthAnchor, multiplier: 1.0),
            footerContainer.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.2)
            ])
    }

    private func setupCreateAccount() {
        self.footerContainer.addSubview(createAccountContainer)
        createAccountContainer.addSubview(createAccountLabel)
        NSLayoutConstraint.activate([
            createAccountContainer.leadingAnchor.constraint(equalTo: self.footerContainer.leadingAnchor, constant: 24),
            createAccountContainer.trailingAnchor.constraint(equalTo: self.footerContainer.trailingAnchor, constant: -24),
            createAccountContainer.heightAnchor.constraint(equalTo: self.footerContainer.heightAnchor, multiplier: 0.25),
            createAccountContainer.centerYAnchor.constraint(equalTo: self.footerContainer.centerYAnchor)
            ])

        NSLayoutConstraint.activate([
            createAccountLabel.centerXAnchor.constraint(equalTo: self.createAccountContainer.centerXAnchor),
            createAccountLabel.centerYAnchor.constraint(equalTo: self.createAccountContainer.centerYAnchor)
            ])
    }

    @objc func login() {
        let username = userName.text ?? ""
        let passwordEnter = password.text ?? ""
        loginWithCredentials(userName: username, password: passwordEnter)

    }

    private func loginWithCredentials(userName: String, password: String) {
        if userName == "Jorge" && password == "123" {
            navigationController?.pushViewController(HomeAppViewController(), animated: true)
        }
    }
}
