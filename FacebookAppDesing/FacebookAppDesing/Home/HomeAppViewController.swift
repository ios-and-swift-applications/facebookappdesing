//
//  HomeAppViewController.swift
//  FacebookAppDesing
//
//  Created by Jorge luis Menco Jaraba on 23/02/20.
//  Copyright © 2020 Jorge luis Menco Jaraba. All rights reserved.
//

import UIKit

class HomeAppViewController: UIViewController {

    private struct InnerConstants {
        static let historyCellIdentifier = "HistoryCell"
        static let newsCellIdentifier = "NewsCell"
    }

    private let histories: [String] = ["paisaje.jpeg",
        "paisaje2.jpeg",
        "paisaje3.jpeg",
        "paisaje4.jpeg"]

    private let newsArray: [String] = ["1", "2", "4"]

    lazy var homeTitle: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false

        return label
    }()

    lazy var HistoryCollectionview: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let collection = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collection.backgroundColor = .white
        collection.translatesAutoresizingMaskIntoConstraints = false
        return collection
    }()

    lazy var newsCollectionview: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        let collection = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collection.backgroundColor = .brown
        collection.translatesAutoresizingMaskIntoConstraints = false
        return collection
    }()

    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var mainContenView: UIView!
    @IBOutlet weak var historyView: UIView!
    @IBOutlet weak var newsView: UIView!

    init() {
        super.init(nibName: "HomeAppViewController", bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setDelegatesToNewscollectionview()
        setDelegatesToHistorycollectionview()
        registerNewsCollectionViewCell()
        registerHistoryCollectionCell()
    }

    override func loadView() {
        super.loadView()
        setupConstraintNewsComponent()
        setupConstratinsHistoryComponent()
    }

    private func setDelegatesToHistorycollectionview() {
        HistoryCollectionview.delegate = self
        HistoryCollectionview.dataSource = self
    }

    private func setDelegatesToNewscollectionview() {
        newsCollectionview.delegate = self
        newsCollectionview.dataSource = self
    }

    private func registerHistoryCollectionCell() {
        HistoryCollectionview.register(
            HistoryCollectionViewCell.self,
            forCellWithReuseIdentifier: InnerConstants.historyCellIdentifier)
    }

    private func registerNewsCollectionViewCell() {
        newsCollectionview.register(
            NewsCollectionViewCell.self,
            forCellWithReuseIdentifier: InnerConstants.newsCellIdentifier)
    }

    func setupConstratinsHistoryComponent() {
        setupConstraintsHistoryCollectionView()
    }

    func setupConstraintsHistoryCollectionView() {
        historyView.addSubview(HistoryCollectionview)
        NSLayoutConstraint.activate([
            HistoryCollectionview.trailingAnchor.constraint(equalTo: historyView.trailingAnchor,
                constant: -12.0),
            HistoryCollectionview.leadingAnchor.constraint(equalTo: historyView.leadingAnchor,
                constant: 12.0),
            HistoryCollectionview.topAnchor.constraint(equalTo: historyView.topAnchor,
                constant: 12.0),
            HistoryCollectionview.bottomAnchor.constraint(equalTo: historyView.bottomAnchor,
                constant: -12.0)
            ])
    }

    func setupConstraintNewsComponent() {
        setupConstraintsNewsCollectionView()
    }

    func setupConstraintsNewsCollectionView() {
        newsView.addSubview(newsCollectionview)
        NSLayoutConstraint.activate([
            newsCollectionview.trailingAnchor.constraint(equalTo: newsView.trailingAnchor,
                constant: 0.0),
            newsCollectionview.leadingAnchor.constraint(equalTo: newsView.leadingAnchor,
                constant: 0.0),
            newsCollectionview.topAnchor.constraint(equalTo: newsView.topAnchor,
                constant: 0.0),
            newsCollectionview.bottomAnchor.constraint(equalTo: newsView.bottomAnchor,
                constant: 0.0)
            ])
    }

    private func configureHistorieCell(collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = HistoryCollectionview.dequeueReusableCell(
            withReuseIdentifier: InnerConstants.historyCellIdentifier,
            for: indexPath) as? HistoryCollectionViewCell else {
            fatalError("the cell can`t be build")
        }

        let data = histories[indexPath.item]
        cell.setupCell(title: data)
        return cell
    }

    private func configureNewCell(collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = newsCollectionview.dequeueReusableCell(
            withReuseIdentifier: InnerConstants.newsCellIdentifier,
            for: indexPath) as? NewsCollectionViewCell else {
            fatalError("the cell can`t be build")
        }

        let data = newsArray[indexPath.item]
        cell.setupCell(title: data)
        return cell
    }
}

extension HomeAppViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.HistoryCollectionview {
            return CGSize(width: collectionView.frame.width / 3, height: collectionView.frame.width / 2)
        } else {
            return CGSize(width: collectionView.frame.width, height: collectionView.frame.height / 1.5)
        }
    }
}

extension HomeAppViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        return
    }
}

extension HomeAppViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        if collectionView == HistoryCollectionview {
            return histories.count
        } else {
            return newsArray.count
        }
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        if collectionView == newsCollectionview {
            return configureNewCell(collectionView: collectionView, cellForItemAt: indexPath)
        }

        if collectionView == HistoryCollectionview {
            return configureHistorieCell(collectionView: collectionView, cellForItemAt: indexPath)
        }

        return UICollectionViewCell()
    }
}
